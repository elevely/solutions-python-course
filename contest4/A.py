import sys
 
class ExtendedList(list):
    @property
    def reversed(self):
        return self[::-1]
 
    @reversed.setter
    def reversed(self, value):
        pass
 
    @property
    def R(self):
        return self[::-1]
 
    @reversed.setter
    def R(self, value):
        pass
 
    @property
    def first(self):
        return self[0]
 
    @first.setter
    def first(self, value):
        self[0] = value
 
    @property
    def F(self):
        return self[0]
 
    @first.setter
    def F(self, value):
        self[0] = value
 
    @property
    def last(self):
        return self[-1]
 
    @last.setter
    def last(self, value):
        self[-1] = value
 
    @property
    def L(self):
        return self[-1]
 
    @last.setter
    def L(self, value):
        self[-1] = value
 
    @property
    def size(self):
        return len(self)
 
    @size.setter
    def size(self, value):
        if value > len(self):
            for i in range(len(self), value):
                self.append(None)
        elif value < len(self):
            for i in range(value, len(self)):
                self.pop()
 
    @property
    def S(self):
        return len(self)
 
    @size.setter
    def S(self, value):
        if value > len(self):
            for i in range(len(self), value):
                self.append(None)
        elif value < len(self):
            for i in range(value, len(self)):
                self.pop()
 
a = ExtendedList([1, 2, 3])
b = ExtendedList([4, 5, 6])
c = ExtendedList([7, 8, 9])
abc = ExtendedList([a, b, c])
print(abc)
print(abc.R)