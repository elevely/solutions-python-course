import json

max_health = {"Sorceress": 50, "Knight": 100,
              "Barbarian": 120, "Warlock": 70}
max_defence_power = {"Sorceress": 42, "Knight": 170,
                     "Barbarian": 150, "Warlock": 50}
max_attack_power = {"Sorceress": 90, "Knight": 150,
                    "Barbarian": 180, "Warlock": 100}
max_mana = {"Sorceress": 200, "Knight": None,
            "Barbarian": None, "Warlock": 180}


def get_race(unit_id):
    return model['armies'][unit_id]["race"]


def get_attack(unit_id):
    return model['armies'][unit_id]["attack"]


def get_health(unit_id):
    return model["armies"][unit_id]['health']


def get_defence(unit_id):
    return model["armies"][unit_id]['defence']


def search_winner(model):
    summ_archi = 0
    summ_roni = 0
    archi_live = False
    roni_live = False
    for unit_id in model['armies']:
        unit = model['armies'][unit_id]
        summ = 0
        if unit["health"] > 0:
            summ += unit['experience'] + 2 * unit['defence'] + \
                    3 * unit['attack'] + 10 * unit.get('mana', 0)
            if unit['lord'] == "Archibald":
                summ_archi += summ
                archi_live = True
            else:
                summ_roni += summ
                roni_live = True
    if summ_archi > summ_roni:
        return "Archibald"
    elif summ_roni == summ_archi:
        return "unknown"
    else:
        return "Ronald"


data = input()
model = json.loads(data)

for action in model['battle_steps']:
    executor = model['armies'][action['id_from']]
    recipient = model['armies'][action['id_to']]
    if recipient['health'] > 0 and executor['health'] > 0:
        if action['action'] == 'cast_damage_spell':
            difference = get_defence(action['id_to']) - action["power"]
            if difference > 0:
                recipient['defence'] = difference
            else:
                recipient['defence'] = 0
                recipient['health'] = max(0, recipient['health'] +
                                          difference)
            executor['mana'] -= action["power"]
            if recipient['health'] > 0:
                executor['experience'] += 1
                recipient['experience'] += 1
            else:
                executor['experience'] += 5
        elif action['action'] == 'attack':
            difference = get_defence(action['id_to']) - action['power']
            if difference > 0:
                recipient['defence'] = difference
                executor['attack'] -= action['power']
            else:
                recipient['defence'] = 0
                recipient['health'] = max(0, recipient['health'] +
                                          difference)
            if recipient['health'] > 0:
                executor['experience'] += 1
                recipient['experience'] += 1
            else:
                executor['experience'] += 5
        elif action['action'] == 'cast_health_spell':
            recipient['health'] = min(max_health[get_race(action["id_to"])],
                                      get_health(action["id_to"]) +
                                      action["power"])
            executor['mana'] -= action["power"]
            executor['experience'] += 1
    model['armies'][action['id_from']] = executor
    model['armies'][action['id_to']] = recipient

print(search_winner(model))
