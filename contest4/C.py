class Player:
    def ready(self, change_start, change_ready):
        if self.flag == True:
            setattr(self, change_ready, getattr(self, self.last_ready_name))
            delattr(Player, self.last_ready_name)
            self.flag = False
        else:
            setattr(self, change_ready, getattr(self, self.last_ready_name))
            delattr(self, self.last_ready_name)

        self.last_ready_name = change_ready
        self.his_game.current_start = change_start


def play(game):
    new_player = Player()
    new_player.last_ready_name = "ready"
    new_player.his_game = game
    new_player.flag = True
    new_player.his_game.current_start = "start"
    while True:
        getattr(game, new_player.his_game.current_start)(new_player)
