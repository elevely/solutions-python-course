def cache(size):

    from collections import OrderedDict
    from functools import wraps

    def decorator(func):
        @wraps(func)
        def wrapper(*data, **named_data):
            if (data, str(named_data)) in res.keys():
                res.move_to_end((data, str(named_data)))
                return res[(data, str(named_data))]
            if len(res) >= size:
                res.popitem(last=False)
            res[(data, str(named_data))] = func(*data, **named_data)
            res.move_to_end((data, str(named_data)))
            return res[(data, str(named_data))]
        res = OrderedDict()
        return wrapper
    return decorator
