from __future__ import print_function
import sys
from functools import wraps


def takes(*args):
    def decorator(func):
        @wraps(func)
        def wrapped(*data):
            for i in range(min(len(args), len(data))):
                if not (isinstance(data[i], args[i])):
                    raise TypeError
            return func(*data)

        return wrapped

    return decorator


exec(sys.stdin.read())
