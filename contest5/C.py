def profiler(func):

    from functools import wraps
    import time

    @wraps(func)
    def wrapped(*data, **named_data):
        if wrapped.evenwork == None:
            wrapped.evenwork = (data, named_data)
            wrapped.calls = 0
        wrapped.calls += 1
        start_time = time.clock()
        res = func(*data)
        finish_time = time.clock()
        wrapped.last_time_taken = finish_time - start_time
        if wrapped.evenwork == (data, named_data):
            wrapped.evenwork = None
        return res
    wrapped.evenwork = None
    wrapped.last_time_taken = 0
    wrapped.calls = -1
    return wrapped

@profiler
def Akkerman(m, n):
  if m == 0:
    return n + 1
  elif n == 0:
    return Akkerman(m - 1, 1)
  else:
    return Akkerman(m - 1, Akkerman(m, n - 1))
 
print(Akkerman(3, 5))
print(Akkerman.last_time_taken)
print(Akkerman.calls)