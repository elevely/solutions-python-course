import math


class Range:
    def __init__(self, one, two=None, step_=None):
        if two is None and step_ is None:
            self.start = 0
            self.end = one
            self.step = 1
        elif step_ is None:
            self.start = one
            self.end = two
            self.step = 1
        elif two is not None and step_ is not None:
            self.start = one
            self.end = two
            self.step = step_
        self.range = range(self.start, self.end, self.step)

    def __contains__(self, item):
        return self.range.__contains__(item)

    def __getitem__(self, item):
        return self.range.__getitem__(item)

    def __repr__(self):
        return self.range.__repr__()

    def __len__(self):
        return self.range.__len__()
