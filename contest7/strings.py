def strings(filename, min_str_len=4):
    import re
    import string

    text = list()
    printable_chars = set(bytes(string.printable, "ascii"))

    with open(filename, 'rb') as file:
        for line in file:
            # print(line.decode("UTF-8"))
            quote = ""
            for i in line:
                if i in printable_chars:
                    quote += chr(i)
                else:
                    if (len(quote) > min_str_len):
                        text.append(quote)
                    quote = ""
            if (len(quote) > min_str_len):
                text.append(quote)

    for s in text:
        if len(s) > min_str_len:
            yield s

# strings("scoring_network_dumb.bin", 2)
