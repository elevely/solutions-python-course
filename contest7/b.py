from collections import Iterable


def flatit(obj, ignore_types=(str, bytes)):
    for new_obj in obj:
        if isinstance(new_obj, str):
            for i in new_obj:
                yield i
        if isinstance(new_obj, Iterable) and\
                not isinstance(new_obj, ignore_types):
            yield from flatit(new_obj)
        else:
            if not isinstance(new_obj, str):
                yield new_obj

