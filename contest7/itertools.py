def dict_merge(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def uniq(vehicles):
    uniquekeys = {}
    for i in vehicles:
        if i not in uniquekeys:
            uniquekeys[i] = 1
    return list(uniquekeys.keys())


def product(vector1, vector2):
    from itertools import starmap
    from itertools import zip_longest

    def add(a, b):
        return a*b
    sum = 0
    for item in starmap(add, zip_longest(vector1, vector2, fillvalue=0)):
        sum += item
    return sum


def transpose(vector):
    list_ = []
    from itertools import starmap
    from itertools import zip_longest
    for y in zip_longest(*vector, fillvalue=0):
        list_.append(y)
    return list_

