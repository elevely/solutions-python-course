def prepare_to_british_scientists(text, letters_to_shuffle_nm):
    import re
    import random

    def shuffle_words(words):
        for i in range(0, len(words)):
            if len(words[i]) <= 2:
                continue
            elif letters_to_shuffle_nm > len(words[i]) - 2:
                letters_to_shuffle = range(1, len(words[i]) - 1)
            else:
                letters_to_shuffle =\
                    random.sample(
                        range(1, len(words[i]) - 1), letters_to_shuffle_nm)

            new_letters_order =\
                random.sample(letters_to_shuffle, len(letters_to_shuffle))
            new_word = list(words[i])

            for letter_nm in range(0, len(letters_to_shuffle)):
                new_word[new_letters_order[letter_nm]] =\
                    words[i][letters_to_shuffle[letter_nm]]
            words[i] = ''.join(new_word)
            print(words[i])
        return

    words = re.findall("\w+", text)
    gaps = re.findall("\W+", text)
    shuffle_words(words)

    new_text = ""

    for i in range(0, len(words)):
        new_text += words[i] + gaps[i]

    return new_text
