class rgb_format(object):
    """docstring for rgb_format"""

    def __init__(self, text):
        self.rgb = list()
        self.rgb_state = dict()
        self.parse_str(text)

    def __str__(self):
        return ' '.join([str(i) for i in self.rgb])

    def parse_str(self, text):
        text = ''.join(text.split())
        try:
            if text[0] == '#':
                for i in range(1, len(text), 2):
                    self.rgb.append(
                        int(text[i], 16) * 16 + int(text[i + 1], 16))
            elif text[0] == 'r' or text[0] == 'g' or text[0] == 'b':
                for i in range(0, 3):
                    self.rgb_state[text[i]] = i
                rgb_formula = text[4:-1].split(',')
                if len(rgb_formula) > 3:
                    raise IndexError
                for i in range(0, 3):
                    self.rgb_state[text[i]] = i
                self.rgb.append(rgb_formula[self.rgb_state['r']])
                self.rgb.append(rgb_formula[self.rgb_state['g']])
                self.rgb.append(rgb_formula[self.rgb_state['b']])

                if self.rgb[i][-1] == '%':
                    for i in range(0, len(self.rgb)):
                        self.rgb[i] = int(int(self.rgb[i][:-1]) * 255 / 100)
                else:
                    for i in range(0, len(self.rgb)):
                        self.rgb[i] = int(self.rgb[i])
            else:
                self.rgb = [int(value) for value in text.split(',')]

            for value in self.rgb:
                if value > 255 or value < 0:
                    raise ValueError

            print(self)
        except Exception as e:
            print("ERROR")


text = input()
rgb_format(text)
