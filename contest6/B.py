
def prepare_to_british_scientists(text, letters_to_shuffle_nm):
    import re
    import random as rnd

    def swap(word, one, two):
        a = ''
        for i in range(len(word)):
            if i == one:
                a += word[two]
            elif i == two:
                a += word[one]
            else:
                a += word[i]
        return a

    def shuffle_letter(word, letters_to_shuffle_nm):
        if len(word) > 2:
            for i in range(letters_to_shuffle_nm):
                word = swap(word, rnd.randint(1, len(word) - 2),
                            rnd.randint(1, len(word) - 2))
        return word

    list_text = []
    for line in text:
        list_text.append(line)
    for i in range(len(list_text)):
        if list_text[i][-1] == "\n":
            list_text[i] = list_text[i][0:len(list_text[i]) - 1]

    for i in range(len(list_text)):
        list_text[i] = re.split(r' ', list_text[i])

    for i in range(len(list_text)):
        for j in range(len(list_text[i])):
            if not ('\'' in list_text[i][j] or '--' in list_text[i][j]):
                if list_text[i][j].isalpha():
                    list_text[i][j] = shuffle_letter(
                        list_text[i][j][0:len(list_text[i][j]):1],
                        letters_to_shuffle_nm)
                elif list_text[i][j][len(list_text[i][j])
                                     - 1] in "--!,.\";:?*":
                    if list_text[i][j][0] in "--!,.\";:?*":
                        list_text[i][j] = list_text[i][j][0] + shuffle_letter(
                            list_text[i][j][1:len(list_text[i][j]) - 1:1],
                            letters_to_shuffle_nm) + list_text[i][j][-1]
                    else:
                        list_text[i][j] = shuffle_letter(list_text[i][j][0:len(
                            list_text[i][j]) - 1:1], letters_to_shuffle_nm) +\
                            list_text[i][j][-1]
                elif list_text[i][j][0] in "--!,.\";:?*":
                    list_text[i][j] = list_text[i][j][0] + \
                        shuffle_letter(
                            list_text[i][j][1:len(list_text[i][j]):1],
                            letters_to_shuffle_nm)

    for line in list_text:
        for word in line:
            print(word, end=" ")
        print()


if __name__ == '__main__':
    main()

# text = open('./text.txt', 'r')
# prepare_to_british_scientists(text, 2)
