import traceback
import sys


def force_load(file_wtihout_extension):

    file = open(file_wtihout_extension + '.py')
    text = file.readlines()
    notExecutable = False
    while (not notExecutable):
        locals_dict = {}
        notExecutable = True
        try:
            exec(''.join(text), globals(), locals_dict)
        except SyntaxError as err:
            notExecutable = False
            del text[err.lineno - 1]
        except Exception as err:
            className, notExecutable, trace = sys.exc_info()
            notExecutable = False
            delete_line_num = traceback.extract_tb(trace)[-1][1]
            del text[delete_line_num - 1]
    return locals_dict
